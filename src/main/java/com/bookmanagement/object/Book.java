package com.bookmanagement.object;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 * Created by khalid on 6/6/16.
 */
public class Book {
    private String title;
    private String author;
    private String publisher;
    private String date;
    private Long isbn;
    private static int count=0;
    Book() {}

    public Book (JSONObject jobj) {
        for (String key : jobj.keySet()) {
            switch (key) {
                case "title": {
                    System.out.println("Got title");
                    this.setTitle(jobj.getString(key));
                    break;
                }
                case "author": {
                    System.out.println("Got Author");
                    this.setAuthor(jobj.getString(key));
                    break;
                }
                case "publisher": {
                    System.out.println("Got Publisher");
                    this.setPublisher(jobj.getString(key));
                    break;
                }
                case "date": {
                    System.out.println("Got Date");
                    this.setDate(jobj.getString(key));
                    break;
                }
                case "isbn": {
                    System.out.println("Got ISBN");
                    this.setIsbn(jobj.getLong(key));
                    break;
                }
                default:{
                    break;
                }
            }
        }
    }

    public void setBookDetails(JSONObject jobj) {
        for (String key : jobj.keySet()) {
            switch (key) {
                case "title": {
                    System.out.println("Got title");
                    this.setTitle(jobj.getString(key));
                    break;
                }
                case "author": {
                    System.out.println("Got Author");
                    this.setAuthor(jobj.getString(key));
                    break;
                }
                case "publisher": {
                    System.out.println("Got Publisher");
                    this.setPublisher(jobj.getString(key));
                    break;
                }
                case "date": {
                    System.out.println("Got Date");
                    this.setDate(jobj.getString(key));
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    public JSONObject toJSON() {
        //TODO write logic to convert a book to json format
        JSONObject jobj = new JSONObject();
        jobj.put("title",this.getTitle());
        jobj.put("publisher",this.getPublisher());
        jobj.put("author",this.getAuthor());
        jobj.put("date",this.getDate());
        jobj.put("isbn",this.getIsbn());
        return jobj;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    public String getAuthor() {
        return author;
    }

    public void setPublisher(String publisher){
        this.publisher = publisher;
    }
    public String getPublisher(){
        return publisher;
    }

    public void setDate(String date){
        this.date = date;
    }
    public String getDate(){
        return date;
    }

    public void setIsbn(Long isbn){
        this.isbn = isbn;
    }
    public Long getIsbn(){
        return  isbn;
    }


}

