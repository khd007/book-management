package com.bookmanagement.api;

import com.bookmanagement.object.Book;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by khalid on 6/8/16.
 */
public class BookResource {
    private static BookResource br = null;
    protected BookResource(){}
    public static BookResource getInstance(){
        if(br == null) {
            br = new BookResource();
        }
        return br;
    }
    List<Book> books = new ArrayList<>();

    void createBook(JSONObject jobj) {
        Book newBook = new Book(jobj);
        books.add(newBook);
        for (Book b : books) {
            System.out.println(b.toJSON());
        }
    }

    JSONObject getBook(Long Isbn){
        JSONObject jobj = new JSONObject();
        for(Book book:books){
            if(book.getIsbn().equals(Isbn)) {
                jobj = book.toJSON();
                break;
            }
        }
        return jobj;
    }
    void updateBook(JSONObject jobj, Long Isbn){
        for(Book book:books){
            if(book.getIsbn().equals(Isbn)) {
                book.setBookDetails(jobj);
                break;
            }
        }
    }

    JSONArray getBooks() {
        JSONArray jarr = new JSONArray();
        for(Book book: books ) {
            System.out.println(book.toJSON());
            jarr.put(book.toJSON());
        }
        return jarr;
    }
}