package com.bookmanagement.api;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("books")

public class BookController {

    private static final String COUNT = "count";
    private static final String RESULT = "result";
    private static final String TIME_TAKEN = "timeTaken";
    private static final String MESSAGE = "message";


    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
//Get method to parse .
    public Response getJsonBooks() throws IOException {
        double startTime = System.currentTimeMillis();
        BookResource br = BookResource.getInstance();
       System.out.println("Returning " + br.getBooks().length() + " book(s).");
        JSONArray jarr = br.getBooks();
        double endTime = System.currentTimeMillis();
        double timeTaken = (endTime - startTime);

        JSONObject result = new JSONObject();
        result.put(RESULT, jarr);
        result.put(COUNT, jarr.length());
        result.put(TIME_TAKEN, timeTaken);

        return Response.ok(result.toString(2)+"\n").build();
    }

    @GET
    @Path("/{isbn}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJsonBook(@PathParam("isbn") Long Isbn) {
        long startTime = System.currentTimeMillis();
        BookResource br = BookResource.getInstance();
        long endTime = System.currentTimeMillis();
        long timeTaken = (endTime - startTime);

        JSONObject result = new JSONObject();
        result.put(RESULT, br.getBook(Isbn));
        result.put(COUNT, 1);
        result.put(TIME_TAKEN, timeTaken);

        return Response.ok(result.toString(2)+"\n").build();
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Post method takes string argument in form of json and calls createBook method of BookResource
    public Response setJsonBooks(String inputBook) throws IOException {
        long startTime = System.currentTimeMillis();
        BookResource br = BookResource.getInstance();
        JSONObject jsObj = new JSONObject(inputBook);
        System.out.println(jsObj.toString(2));
        br.createBook(jsObj);
        long endTime = System.currentTimeMillis();

        long timeTaken = (endTime - startTime);

        JSONObject result = new JSONObject();
        result.put(MESSAGE, "success");
        result.put(TIME_TAKEN, timeTaken);
        return Response.ok(result.toString(2)+"\n").build();
    }

    @PUT
    @Path("/{isbn}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateJsonBook(String inputDetails, @PathParam("isbn") Long Isbn) {
        long startTime = System.currentTimeMillis();
        BookResource br = BookResource.getInstance();
        JSONObject jobj = new JSONObject(inputDetails);
        br.updateBook(jobj, Isbn);
        long endTime = System.currentTimeMillis();
        Long timeTaken = (endTime - startTime);

        JSONObject result = new JSONObject();
        result.put(MESSAGE, "success");
        result.put(TIME_TAKEN, timeTaken);

        return Response.ok(result.toString(2)+"\n").build();
    }
}
