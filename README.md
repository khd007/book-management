Project-BookManagement
======================
Description:
------------
This project is about managing books using the concept of RESTful Web Services. This project is build in **Maven** as webapp application and it is designed using **IntellijIdea** Ide in combination using **Jersey** framework and **Jetty** as a plugin server. I have build this project on *MVC* architecture.

Motivation:
----------
I started this project as part of my training so that i can understand how real project are build and deployed.

Acknowledgments:
----------------
[Avin D'Silva](https://gitlab.com/u/avindsilva)
Helped me a lot to build this project and i am thankful to him.

Contact:
--------
Author:Khalid Hamid
E-mail:danishkhalid04@gmail.com

